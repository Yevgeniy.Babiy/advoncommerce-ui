type User = {
  user_id: number;
  role_id: number;
};

declare namespace Express {
  export interface Request {
    auth?: User;
    file?: any;
    files?: any;
  }
}