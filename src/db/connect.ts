/**
 * @type {Knex}
 */

import knex from 'knex';
import dotenv from "dotenv";

dotenv.config();

const client = process.env.DB_CONNECTION;
const host = process.env.DB_HOST;
const port = Number(process.env.DB_PORT);
const user = process.env.DB_USER;
const password = process.env.DB_PASSWORD;

export const database = () => {
    return knex({
        client,
        connection: {
            host,
            port,
            user,
            password,
            database : 'amazon_products.sql'
        }
    });
}



