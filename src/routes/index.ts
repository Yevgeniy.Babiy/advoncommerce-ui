import { Router } from 'express';
import { items } from '../items';
import {database} from "../db/connect";
const db = database();

export  function rootRoutes (): Router {
  const router: Router = Router({ mergeParams: true });

  router.get('/catalog', async function(req, res) {
    try {
      // const items = await db.column('id', 'asin', 'page_url', 'img_large', 'brand', 'price', 'title',).select().from('products');
      res.render("catalog", {
        title: "Phones",
        items: items,
        isCatalog: true
      });
    } catch (e) {
      console.log(e);
    }
  });

  router.get('/product_details', function(req, res) {
    res.render("product_details", {
      items: items[0],
      isSingleProduct: true
    });
  });

  router.get('/about', function(req, res) {
    res.render("about", {
      title: "Мои контакты",
      email: "gavgav@mycorp.com",
      phone: "+1234567890"
    });
  });

  router.get('/faq', function(req, res) {
    res.render("faq");
  });

  router.get('/contacts', function(req, res) {
    res.render("contacts", {
      title: "Contacts",
      email: "productpick@gmail.com",
      phone: "+1234567890"
    });
  });

  router.use("/", function(_, res){
    res.render("home", {
      isHome: true,
      exclusive: items,
    });
  });

  return router;
}