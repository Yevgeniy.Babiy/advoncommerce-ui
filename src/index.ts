import * as dotenv from "dotenv";
import express from 'express';
import hbs from 'hbs';
import { engine } from 'express-handlebars';
import cors from "cors";
const sassMiddleware = require('node-sass-middleware');
import path from 'path';
import {rootRoutes} from "./routes";

dotenv.config();

const PORT = process.env.PORT || 3000;
const app: express.Express = express();

app.use(cors());
app.use(express.json());

app.set('views', path.join(__dirname, 'views'));
app.engine("hbs", engine(
    {
        layoutsDir: path.join(__dirname, "views/layouts"),
        defaultLayout: "layout",
        extname: "hbs"
    }
));
app.set('view engine', 'hbs');
hbs.registerPartials(path.join(__dirname, "views/partials"));

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(cookieParser());


app.use(sassMiddleware({
  src: path.join(__dirname, 'assets'),
  dest: 'public',
  debug: true,
  force: true,
  outputStyle: 'compressed',
  // prefix:  '/src'
  // Where prefix is at <link rel="stylesheets" href="prefix/style.css"/>
}));

app.use(express.static("public"));

app.use(rootRoutes());

app.listen(PORT, ()=>{
    console.log(`>>> Server has been started on port ${PORT}`);
})