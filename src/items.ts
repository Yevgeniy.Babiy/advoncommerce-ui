export const items = [
  {
    "ASIN": "B07ZQRN3KR",
    "DetailPageURL": "https://www.amazon.com/dp/B07ZQRN3KR?tag=dwym-20&linkCode=osi&th=1&psc=1",
    "Images": {
      "Primary": {
        "Large": {
          "Height": 500,
          "URL": "https://m.media-amazon.com/images/I/41VVmFprs6L._SL500_.jpg",
          "Width": 373
        }
      }
    },
    "ItemInfo": {
      "Title": {
        "DisplayValue": "Apple iPhone 11 Pro, US Version, 256GB, Silver - Unlocked (Renewed)",
        "Label": "Title",
        "Locale": "en_US"
      }
    },
    "Offers": {
      "Listings": [
        {
          "Id": "ubn%2FCCXzxEkkduR3KroVDKw3zbHFI80xy1z5YiNbC4aTn2aP3QK%2BEZehQDJiliiYckkhhmjczCJe7HM0ZS2kxMtYcp61hBjXuijp97lSHMfkxMfESIVnW3%2BIRFJMqFtMrgJGW5W88JxfLkj0tx23UsvwMIDbPL4l",
          "Price": {
            "Amount": 513.99,
            "Currency": "USD",
            "DisplayAmount": "$513.99",
            "Savings": {
              "Amount": 84.01,
              "Currency": "USD",
              "DisplayAmount": "$84.01 (14%)",
              "Percentage": 14
            }
          },
          "ViolatesMAP": false
        }
      ]
    }
  },
  {
    "ASIN": "B07ZPHSHPG",
    "DetailPageURL": "https://www.amazon.com/dp/B07ZPHSHPG?tag=dwym-20&linkCode=osi&th=1&psc=1",
    "Images": {
      "Primary": {
        "Large": {
          "Height": 500,
          "URL": "https://m.media-amazon.com/images/I/41NsNJqhw1L._SL500_.jpg",
          "Width": 383
        }
      }
    },
    "ItemInfo": {
      "Title": {
        "DisplayValue": "Apple iPhone 11, 64GB, Green - Unlocked (Renewed)",
        "Label": "Title",
        "Locale": "en_US"
      }
    },
    "Offers": {
      "Listings": [
        {
          "Id": "ubn%2FCCXzxEkkduR3KroVDPE4V1GD8Jeh5TVdIp2woy391EKPzps3wsCfkEelmJNtL0J6KkQSdZegFbO%2FeYxMhD1X3F7bPXhONuCFgFz3ng7sLioyVOZ17oq0YLyFe3Yj7ZDHSVRzzJwMH5ehw39fO4qDc8G%2FyExs2NgfNr%2BlHnU%3D",
          "Price": {
            "Amount": 375,
            "Currency": "USD",
            "DisplayAmount": "$375.00",
            "Savings": {
              "Amount": 64,
              "Currency": "USD",
              "DisplayAmount": "$64.00 (15%)",
              "Percentage": 15
            }
          },
          "ViolatesMAP": false
        }
      ]
    }
  },
  {
    "ASIN": "B07P978C2R",
    "DetailPageURL": "https://www.amazon.com/dp/B07P978C2R?tag=dwym-20&linkCode=osi&th=1&psc=1",
    "Images": {
      "Primary": {
        "Large": {
          "Height": 400,
          "URL": "https://m.media-amazon.com/images/I/41ZjUOH6nRL._SL500_.jpg",
          "Width": 500
        }
      }
    },
    "ItemInfo": {
      "Title": {
        "DisplayValue": "(Renewed) Apple iPhone XR, US Version, 64GB, White - Unlocked",
        "Label": "Title",
        "Locale": "en_US"
      }
    },
    "Offers": {
      "Listings": [
        {
          "Id": "ubn%2FCCXzxEkkduR3KroVDPjbV3HaGzh3SIqimugEkeNYQsOUYj2REzuvS9F7E6slRDp%2FM2yJ7RXos3xHgdFRh6kOXbVX12lV1uhbihogczhGJt20BM8HbfWdPY%2FV2CtU1%2BjpNZfsmtqV%2B3ks%2BjTM%2BqljvWuoEDck",
          "Price": {
            "Amount": 282,
            "Currency": "USD",
            "DisplayAmount": "$282.00",
            "Savings": {
              "Amount": 36.95,
              "Currency": "USD",
              "DisplayAmount": "$36.95 (12%)",
              "Percentage": 12
            }
          },
          "ViolatesMAP": false
        }
      ]
    }
  },
  {
    "ASIN": "B07K4V35TJ",
    "DetailPageURL": "https://www.amazon.com/dp/B07K4V35TJ?tag=dwym-20&linkCode=osi&th=1&psc=1",
    "Images": {
      "Primary": {
        "Large": {
          "Height": 500,
          "URL": "https://m.media-amazon.com/images/I/31B6MQRxUdL._SL500_.jpg",
          "Width": 267
        }
      }
    },
    "ItemInfo": {
      "Title": {
        "DisplayValue": "(Renewed) Apple iPhone XS, 64GB, Gold - Fully Unlocked",
        "Label": "Title",
        "Locale": "en_US"
      }
    },
    "Offers": {
      "Listings": [
        {
          "Id": "ubn%2FCCXzxEkkduR3KroVDNrNjoX8b2347lmxYTa2pYxkTonBo57gKVYMDUSNjUpaIAGjXPe65saJZMtaT8z4BqSiEVNbzhm%2BoAi%2F3aaa9OrylivV64WIFb8z8vSu2pKHFW2oAFcIioM3MyS5cqRPBzDHrXwynT19",
          "Price": {
            "Amount": 279.75,
            "Currency": "USD",
            "DisplayAmount": "$279.75",
            "Savings": {
              "Amount": 54.25,
              "Currency": "USD",
              "DisplayAmount": "$54.25 (16%)",
              "Percentage": 16
            }
          },
          "ViolatesMAP": false
        }
      ]
    }
  },
  {
    "ASIN": "B09JFNMBWL",
    "DetailPageURL": "https://www.amazon.com/dp/B09JFNMBWL?tag=dwym-20&linkCode=osi&th=1&psc=1",
    "Images": {
      "Primary": {
        "Large": {
          "Height": 500,
          "URL": "https://m.media-amazon.com/images/I/31yRKy7VU2L._SL500_.jpg",
          "Width": 500
        }
      }
    },
    "ItemInfo": {
      "Title": {
        "DisplayValue": "Apple iPhone 12 Pro, 128GB, Pacific Blue - Unlocked (Renewed Premium)",
        "Label": "Title",
        "Locale": "en_US"
      }
    },
    "Offers": {
      "Listings": [
        {
          "Id": "ubn%2FCCXzxEk0W%2FEYYb2hxeSy1bYK95A4DYVJsvuLEq21nv5HltRuilQpJLoeCXmqUemq7YAIgiu9QpgE1WcQAi2uOAGDKKLTKbO%2BYMqM87DJJV2MLZyEfQ%3D%3D",
          "Price": {
            "Amount": 849,
            "Currency": "USD",
            "DisplayAmount": "$849.00",
            "Savings": {
              "Amount": 50,
              "Currency": "USD",
              "DisplayAmount": "$50.00 (6%)",
              "Percentage": 6
            }
          },
          "ViolatesMAP": false
        }
      ]
    }
  }
]

